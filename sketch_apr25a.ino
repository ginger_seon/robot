int in1 = 13;
int in2 = 12;
int in3 = 4;
int in4 = 16;



void setup() {
  pinMode(in1, OUTPUT);
  pinMode(in2, OUTPUT);
  pinMode(in3, OUTPUT);
  pinMode(in4, OUTPUT);
  Serial.begin(9600);
}

void loop() {
  if(Serial.available()){
    char in_data;
    in_data = Serial.read();
    //forward
    if(in_data == '1'){
      digitalWrite(in1, HIGH);
      digitalWrite(in2, LOW);
      
      digitalWrite(in3, HIGH);
      digitalWrite(in4, LOW);
    }
    //right
    else if(in_data == '2'){
      
      digitalWrite(in1, LOW);
      digitalWrite(in2, HIGH);
      
      digitalWrite(in3, HIGH);
      digitalWrite(in4, LOW);
    }
    //left
    else if(in_data == '3'){
      
      digitalWrite(in3, LOW);
      digitalWrite(in4, HIGH);
      
      digitalWrite(in1, HIGH);
      digitalWrite(in2, LOW);
    }
    //back
    else if(in_data == '4'){     
      digitalWrite(in3, LOW);
      digitalWrite(in4, HIGH);
      
      digitalWrite(in1, LOW);
      digitalWrite(in2, HIGH);
    }
    else{
      digitalWrite(in1, LOW);
      digitalWrite(in2, LOW);
      digitalWrite(in3, LOW);
      digitalWrite(in4, LOW);
    }
  }
}
